# Cosmin Ciobanu

## About me
Hi! My name is Cosmin Ciobanu and I am a beginning software developer. At this point I would like to share with you my experience and projects I had the pleasure to work on so far.
<center>

</center>

## Events app
This is my final project during the SDA Academy course Java from scratch.
The goal is to create a website that will allow the organizers to enter events. The website should also be equipped with an event search engine (with several criteria).

## Main system functions
* User registration and login.
* Events created and edited by organizers (user with special role).
* Commenting on events by userswho are logged in.
* Signing up for events.
* Event search engine.

package com.events.controllers;

import com.events.model.Comment;
import com.events.services.CommentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/comments")
public class CommentController {
    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/allByEventId/{id}")
    public ResponseEntity<List<Comment>> getCommentByEventId(@PathVariable Long id) {
        Optional<List<Comment>> comments = commentService.findAllCommentsByEventId(id);
        if (comments.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(comments.get());
        }
    }

    @PostMapping("/saveComment")
    @PreAuthorize( "hasAuthority('GUEST') or hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<Comment> saveComment(@RequestBody Comment comment) {
        Comment savedComment = commentService.saveComment(comment);
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(savedComment);
    }

    @DeleteMapping("/deleteCommentById/{id}")
    @PreAuthorize( "hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<String> deleteCommentById(@PathVariable Long id){
        String response = commentService.deleteCommentWitId(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }
}

package com.events.controllers;

import com.events.model.Event;
import com.events.services.EventService;
import com.events.services.PosterService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/events")
public class EventController {
    private final EventService eventService;
    private final PosterService posterService;

    public EventController(EventService eventService, PosterService posterService) {
        this.eventService = eventService;
        this.posterService = posterService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Event>> getAllEvents() {
        List<Event> events = eventService.findAllEvents();
        if (events.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(events);
        }
    }

    @GetMapping("/homePage")
    public ResponseEntity<List<Event>> getHomePageEvents(){
        List<Event> allEvents = eventService.findAllEvents();
        List<Event> resultEvents;
        if (allEvents.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            resultEvents = allEvents.stream()
                    .sorted(Comparator.comparing(Event::getStartDate))
                    .filter(event -> event.getStartDate().isAfter(LocalDateTime.now()))
                    .limit(4)
                    .collect(Collectors.toList());
            return ResponseEntity.status(HttpStatus.OK).body((resultEvents));
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Event> getEventById(@PathVariable Long id) {
        Optional<Event> event = eventService.findEventById(id);
        if (event.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(event.get());
        }
    }

    @GetMapping("/title/{title}")
    public ResponseEntity<List<Event>> getEventByTitle(@PathVariable String title) {
        Optional<List<Event>> events = eventService.findEventByTitle(title);
        if (events.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(events.get());
        }
    }

    @GetMapping("/allByUser/{userId}")
    @PreAuthorize( "hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<List<Event>> getEventsByUser(@PathVariable Long userId) {
        Optional<List<Event>> events = eventService.findAllByUser(userId);
        if (events.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(events.get());
        }
    }

    @PostMapping("/saveEvent")
    @PreAuthorize( "hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<Event> saveEvent(@RequestBody Event event) {
        Event savedEvent = eventService.saveEvent(event);
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(savedEvent);
    }

    @DeleteMapping("/deleteEvent/{id}")
    @PreAuthorize( "hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<Event> deleteEvent(@PathVariable Long id) {
        Optional<Event> eventOptional = eventService.findEventById(id);
        String response="";
        if (eventOptional.isPresent()){
            response = eventService.deleteEvent(id);
            Long posterId = eventOptional.get().getPoster().getPosterId();
            posterService.deletePoster(posterId);
        }
        if (response.equals("not found")) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).build();
        }
    }

    @PutMapping("/updateEvent")
    @PreAuthorize( "hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<Event> updateEvent(@RequestBody Event event) {
        String response = eventService.editEvent(event);
        if (response.equals("event not updated")) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.ACCEPTED).build();
        }
    }
}

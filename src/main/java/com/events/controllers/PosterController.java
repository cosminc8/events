package com.events.controllers;

import com.events.model.Event;
import com.events.model.Poster;
import com.events.services.PosterService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("/poster")
public class PosterController {
    private final PosterService posterService;

    public PosterController(PosterService posterService) {
        this.posterService = posterService;
    }

    @PostMapping("/savePoster")
    @PreAuthorize( "hasAuthority('HOST') or hasAuthority('ADMIN')")
    Poster uploadPoster(@RequestParam("imageFile") MultipartFile file) throws IOException {
        Poster dbPoster = new Poster(file.getBytes(), file.getOriginalFilename());
        return posterService.savePoster(dbPoster);
    }

    @GetMapping(value = "/{id}")
    Poster downloadPoster(@PathVariable Long id) {
        Optional<Poster> poster = posterService.findPosterById(id);
        return poster
                .map(value -> new Poster(value.getPosterId(),
                        value.getContent(),
                        value.getName()))
                .orElse(null);
    }

    @DeleteMapping("/deletePoster/{id}")
    @PreAuthorize( "hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<Poster> deletePoster(@PathVariable Long id){
        Optional<Poster> posterOptional = posterService.findPosterById(id);
        String response="";
        if (posterOptional.isPresent()){
            response = posterService.deletePoster(id);
        }
        if (response.equals("poster not found")) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).build();
        }
    }

}

package com.events.controllers;

import com.events.model.Registration;
import com.events.services.RegistrationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/register")
public class RegistrationController {
    private final RegistrationService registrationService;

    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @GetMapping("/allByUserId/{id}")
    @PreAuthorize( "hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<List<Registration>> getRegistrationByUserId(@PathVariable Long id) {
        Optional<List<Registration>> registrations = registrationService.findRegistrationByUserId(id);
        if (registrations.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(registrations.get());
        }
    }

    @GetMapping("/allByEventId/{id}")
    public ResponseEntity<List<Registration>> getRegistrationByEventId(@PathVariable Long id) {
        Optional<List<Registration>> registrations = registrationService.findRegistrationByEventId(id);
        if (registrations.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(registrations.get());
        }
    }

    @GetMapping("/allByUserAndEvent/{userId}&{eventId}")
    @PreAuthorize( "hasAuthority('GUEST') or hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<Registration> getRegistrationByUserAndEvent(@PathVariable Long userId, @PathVariable Long eventId){
        Optional<Registration> registration = registrationService.findRegistrationByUserAndEvent(userId, eventId);
        if (registration.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(registration.get());
        }
    }

    @PostMapping("/saveRegistration")
    @PreAuthorize( "hasAuthority('GUEST') or hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<Registration> saveRegistration(@RequestBody Registration registration){
        Registration savedRegistration = registrationService.saveRegistration(registration);
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(savedRegistration);
    }

    @DeleteMapping("unregister/{id}")
    @PreAuthorize( "hasAuthority('GUEST') or hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<String> deleteRegistrationById(@PathVariable Long id){
        String response = registrationService.deleteRegistration(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }
}

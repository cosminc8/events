package com.events.controllers;

import com.events.model.Ticket;
import com.events.services.TicketService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tickets")
public class TicketController {
    private final TicketService ticketService;

    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @PostMapping("/buyTicket")
    @PreAuthorize( "hasAuthority('GUEST') or hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<Ticket> buyTicket(@RequestBody Ticket ticket) {
        Ticket savedTicket = ticketService.buyTicket(ticket);
        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(savedTicket);
    }

    @GetMapping("/getByUserId/{id}")
    @PreAuthorize( "hasAuthority('GUEST') or hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<List<Ticket>> getTicketByUserId(@PathVariable Long id) {
        Optional<List<Ticket>> tickets = ticketService.getTicketByUserId(id);
        if (tickets.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(tickets.get());
        }
    }

    @GetMapping("/getByEventId/{id}")
    @PreAuthorize( "hasAuthority('GUEST') or hasAuthority('HOST') or hasAuthority('ADMIN')")
    public ResponseEntity<List<Ticket>> getTicketByEventId(@PathVariable Long id) {
        Optional<List<Ticket>> tickets = ticketService.getTicketByEventId(id);
        if (tickets.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(tickets.get());
        }
    }
}

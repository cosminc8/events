package com.events.enums;

public enum EventStatus {
    DRAFT,
    OPEN_TO_REGISTRATION,
    CLOSED_TO_REGISTRATION,
    ONGOING,
    PAST,
    CANCELLED
}

package com.events.enums;

public enum UserRole {
    ADMIN,
    HOST,
    GUEST
}

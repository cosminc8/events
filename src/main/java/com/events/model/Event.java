package com.events.model;

import com.events.enums.EventStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "events")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "event_id")
    private Long eventId;
    @Column(nullable = false, unique = true)
    private String title;
    @Column(nullable = false)
    private String description;
    @Column(nullable = false)
    private LocalDateTime creationDate;
    @Column(nullable = false)
    private LocalDateTime startDate;
    @Column(nullable = false)
    private LocalDateTime endDate;
    private LocalDateTime registrationEndDate;
    @Enumerated(EnumType.STRING)
    private EventStatus eventStatus;
    @Column(nullable = false)
    private String location;
    private Integer nbrOfPlaces = 0;
    private Double price;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id")
    private User user;
    @OneToMany(mappedBy = "commentEvent", cascade = CascadeType.ALL)
    @ToString.Exclude
    @JsonIgnore
    private List<Comment> commentList;
    @OneToMany(mappedBy = "registrationEvent", cascade = CascadeType.ALL)
    @ToString.Exclude
    @JsonIgnore
    private List<Registration> registrationList;
    @OneToMany(mappedBy = "ticketEvent", cascade = CascadeType.ALL)
    @ToString.Exclude
    @JsonIgnore
    private List<Ticket> ticketList;
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "fk_poster")
    private Poster poster;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(eventId, event.eventId) && Objects.equals(title, event.title) &&
                Objects.equals(description, event.description) && Objects.equals(creationDate, event.creationDate) &&
                Objects.equals(startDate, event.startDate) && Objects.equals(endDate, event.endDate) &&
                Objects.equals(registrationEndDate, event.registrationEndDate) && eventStatus == event.eventStatus &&
                Objects.equals(location, event.location) && Objects.equals(price, event.price) &&
                Objects.equals(user, event.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eventId, title, description, creationDate, startDate, endDate, registrationEndDate,
                eventStatus, location, price, user);
    }
}

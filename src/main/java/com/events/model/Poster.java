package com.events.model;

import lombok.*;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "posters")
public class Poster {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "poster_id")
    private Long posterId;
    @Column(nullable = false)
    @Lob
    byte[] content;
    @Column(nullable = false, unique = true)
    private String name;

    public Poster(byte[] bytes, String originalFilename) {
        this.content = bytes;
        this.name = originalFilename;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Poster poster = (Poster) o;
        return Objects.equals(posterId, poster.posterId) && Arrays.equals(content, poster.content) && Objects.equals(name, poster.name);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(posterId, name);
        result = 31 * result + Arrays.hashCode(content);
        return result;
    }
}

package com.events.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "registrations")
public class Registration {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "registration_id")
    private Long registrationId;
    @Column(nullable = false)
    private LocalDateTime registrationDate;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "event_id")
    private Event registrationEvent;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id")
    private User registrationUser;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Registration that = (Registration) o;
        return Objects.equals(registrationId, that.registrationId) &&
                Objects.equals(registrationDate, that.registrationDate) &&
                Objects.equals(registrationEvent, that.registrationEvent) &&
                Objects.equals(registrationUser, that.registrationUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(registrationId, registrationDate, registrationEvent, registrationUser);
    }
}

package com.events.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "tickets")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ticket_id")
    private Long ticketId;
    @Column(nullable = false)
    private Boolean isPaid = false;
    @Column(nullable = false)
    private LocalDateTime paidDate;
    @Column(nullable = false)
    private int quantity;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "event_id")
    private Event ticketEvent;
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id")
    private User ticketUser;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return isPaid == ticket.isPaid && quantity == ticket.quantity && Objects.equals(ticketId, ticket.ticketId) &&
                Objects.equals(paidDate, ticket.paidDate) && Objects.equals(ticketEvent, ticket.ticketEvent) &&
                Objects.equals(ticketUser, ticket.ticketUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ticketId, isPaid, paidDate, quantity, ticketEvent, ticketUser);
    }
}

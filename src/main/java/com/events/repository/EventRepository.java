package com.events.repository;

import com.events.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    Optional<List<Event>> findByTitleContaining(String title);

    Optional<List<Event>> findByUserUserId(Long userId);
}

package com.events.repository;

import com.events.model.Registration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Long> {
    Optional<List<Registration>> findAllByRegistrationUser_UserId(Long id);

    Optional<List<Registration>> findAllByRegistrationEvent_EventId(Long id);

    Optional<Registration> findAllByRegistrationUser_UserIdAndRegistrationEvent_EventId(Long userId, Long eventId);
}

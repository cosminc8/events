package com.events.repository;

import com.events.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
    Optional<List<Ticket>> findAllByTicketUser_UserId(Long id);

    Optional<List<Ticket>> findAllByTicketEvent_EventId(Long id);
}

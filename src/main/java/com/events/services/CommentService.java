package com.events.services;

import com.events.model.Comment;
import com.events.repository.CommentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentService {
    private final CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public Optional<List<Comment>> findAllCommentsByEventId(Long id){
        return commentRepository.findAllByCommentEvent_EventId(id);
    }

    public Comment saveComment(Comment comment){
        return commentRepository.save(comment);
    }

    public String deleteCommentWitId(Long id){
        Optional<Comment> verifyComment = commentRepository.findById(id);
        if (verifyComment.isEmpty()){
            return "comment not found in DB";
        } else {
            commentRepository.deleteById(id);
            return "comment deleted successfully from DB";
        }
    }
}

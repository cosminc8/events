package com.events.services;

import com.events.model.Event;
import com.events.repository.EventRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EventService {

    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<Event> findAllEvents() {
        return eventRepository.findAll();
    }

    public Optional<Event> findEventById(Long id) {
        return eventRepository.findById(id);
    }

    public Optional<List<Event>> findEventByTitle(String title) {
        return eventRepository.findByTitleContaining(title);
    }

    public Event saveEvent(Event event) {

        return eventRepository.save(event);
    }

    public String deleteEvent(Long id) {
        Optional<Event> verifyEvent = eventRepository.findById(id);
        if (verifyEvent.isEmpty()) {
            return "not found";
        } else {
            eventRepository.delete(verifyEvent.get());
            return "event deleted";
        }
    }

    public String editEvent(Event event) {
        Optional<Event> verifyEvent = eventRepository.findById(event.getEventId());
        if (verifyEvent.isPresent()) {
            eventRepository.save(event);
            return "event updated";
        }
        return "event not updated";
    }

    public Optional<List<Event>> findAllByUser(Long userId) {
        return eventRepository.findByUserUserId(userId);
    }
}

package com.events.services;

import com.events.model.Poster;
import com.events.repository.PosterRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PosterService {
    private final PosterRepository posterRepository;

    public PosterService(PosterRepository posterRepository) {
        this.posterRepository = posterRepository;
    }

    public Optional<Poster> findPosterById(Long id){
        return posterRepository.findById(id);
    }

    public Poster savePoster(Poster poster){
        return posterRepository.save(poster);
    }

    public String deletePoster(Long id){
        Optional<Poster> verifyPoster = posterRepository.findById(id);
        if (verifyPoster.isEmpty()){
            return "poster not found";
        } else {
            posterRepository.delete(verifyPoster.get());
            return "poster deleted";
        }
    }

    public String changePoster(Poster poster){
        Optional<Poster> verifyPoster =  posterRepository.findById(poster.getPosterId());
        if (verifyPoster.isPresent()){
            posterRepository.save(verifyPoster.get());
            return "poster updated";
        } else {
            return "poster not updated";
        }
    }
}

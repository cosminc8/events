package com.events.services;

import com.events.model.Registration;
import com.events.repository.RegistrationRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RegistrationService {
    private final RegistrationRepository registrationRepository;

    public RegistrationService(RegistrationRepository registrationRepository) {
        this.registrationRepository = registrationRepository;
    }

    public Registration saveRegistration(Registration registration){
        return registrationRepository.save(registration);
    }

    public Optional<List<Registration>> findRegistrationByUserId(Long id){
        return registrationRepository.findAllByRegistrationUser_UserId(id);
    }

    public Optional<List<Registration>> findRegistrationByEventId(Long id){
        return registrationRepository.findAllByRegistrationEvent_EventId(id);
    }

    public Optional<Registration> findRegistrationByUserAndEvent(Long userId, Long eventId){
        return registrationRepository.findAllByRegistrationUser_UserIdAndRegistrationEvent_EventId(userId, eventId);
    }

    public String deleteRegistration(Long id){
        Optional<Registration> resultRegistration = registrationRepository.findById(id);
        if (resultRegistration.isEmpty()){
            return "registration not found in DB";
        } else {
            registrationRepository.deleteById(id);
            return "registration deleted from DB";
        }
    }
}

package com.events.services;

import com.events.model.Ticket;
import com.events.repository.TicketRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TicketService {
    private final TicketRepository ticketRepository;

    public TicketService(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    public Ticket buyTicket(Ticket ticket){
        return ticketRepository.save(ticket);
    }

    public Optional<List<Ticket>> getTicketByUserId(Long id){
        return ticketRepository.findAllByTicketUser_UserId(id);
    }

    public Optional<List<Ticket>> getTicketByEventId(Long id){
        return ticketRepository.findAllByTicketEvent_EventId(id);
    }
}
